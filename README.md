# identifiability of the base model

In this project, we implemented the model from Mosca et al.[1].  
A synthetic dataset (directory data/) is used to verify the practical identifiability of the model.  
First, individual estimations have to be made using the CMA-ES algorithm[2] (run CMAES/individual_inference.jl).  
Then, a hierarchical Bayesian inference procedure can be applied (run hierarchical/hierarchical_inference.jl).  
The results are presented in [3].

The code requires the use of following package:  
https://gitlab-research.centralesupelec.fr/2012hermangeg/bayesian-inference  

## References

[1] Mosca,  M.,  Hermange,  G.,  Tisserand,  A.,  Noble,  R.,  Marzac,  C.,  Marty,  C.,Le Sueur, C., Campario, H., Vertenoeil, G., El-Khoury, M., et al. Inferring the dynamics of mutated hematopoietic stem and progenitor cells induced by  IFNα in myeloproliferative neoplasms. Blood(2021)  
[2] Hansen, N.The cma evolution strategy:  A tutorial.arXiv preprint arXiv:1604.00772(2016)  
[3] Hermange, G., Vainchenker, W., Plo, I., & Cournède, P. H. (2021). Mathematical modelling, selection and hierarchical inference to determine the minimal dose in IFNα therapy against Myeloproliferative Neoplasms. arXiv preprint arXiv:2112.10688
