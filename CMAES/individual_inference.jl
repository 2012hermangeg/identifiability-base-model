#First of all, the Bayesian Inference Framework has to be called.
#Core functionnalities from this framework are available at:
#https://gitlab-research.centralesupelec.fr/2012hermangeg/bayesian-inference

include("../../../bayesian-inference/core/environment.jl") #Change the link accordingly


using CSV, DataFrames #To load data

dir_data = "../data/"

#Include the computational model
include("../JAK2V617F_model/computationalModel.jl")
#Include the observation model
include("../observation_model/noiseModel.jl");

#Iterate over all patients from our virtual cohort
for i in 1:30
    
    ######### Load Data ###########################
    name = i

    #Immature cells
    immat = CSV.read(string(dir_data,name,"_immat.csv"), DataFrame) #Data for immature cells - clonal architecture for progenitors
    ti = convert(Vector, immat[!,:t]) #Observation time for immature cells
    ni_het = convert(Vector{Int}, immat[!,:n_het]) #Number of heterozygous mutated cells for sample j
    ni_wt = convert(Vector{Int}, immat[!,:n_wt]) #Number of wildt-type cells for sample j
    ni_hom = convert(Vector{Int}, immat[!,:n_hom]) #Number of homozygous mutated cells for sample j
    ni_tot = ni_het .+ ni_hom .+ ni_wt
    #Mature cells
    mat = CSV.read(string(dir_data,name,"_mat.csv"), DataFrame) #Data form mature cells - VAF among granulocytes
    tm = convert(Vector, mat[!,:t]) #Observation time for mature cells
    ym = convert(Vector, mat[!,:y]) #VAF for mature cells
    ################################################

    #Define the environment variable
    e = newEnvironment();

    #We define the parameters to estimate (and the prior distributions, aka ranges for the parameter values)
    #homozygous
    push!(e["model"]["parameters"], Uniform(1.0, 20.0)) #km hom
    push!(e["model"]["parameters"], Uniform(0.0, 3.0)) #eta hom
    push!(e["model"]["parameters"], Uniform(1.0/300.0, 0.1)) #gamma IFN hom
    push!(e["model"]["parameters"], Uniform(-1.0, 1.0)) #Delta IFN hom
    #heterozygous
    push!(e["model"]["parameters"], Uniform(0.0, 3.0)) #eta het
    push!(e["model"]["parameters"], Uniform(1.0/300.0, 0.1)) #gamma IFN het
    push!(e["model"]["parameters"], Uniform(-1.0, 1.0)); #Delta IFN hom

    #Our computation model
    e["model"]["computational model"] = model;

    #Push observations
    push!(e["data"]["t"], copy(ti))
    push!(e["data"]["t"], copy(ti)) #Same as above, to have 3 time series, as many as model outputs
    push!(e["data"]["t"], copy(tm))
    push!(e["data"]["f"], copy(ym))
    push!(e["data"]["f"], copy(ni_wt))
    push!(e["data"]["f"], copy(ni_het))
    push!(e["data"]["f"], copy(ni_hom));

    #To take into account that some patients might no have any homozygous clones (thus, do not infer parameters related to homozygous cells)
    e["input"]["homozygote"] = maximum(ni_hom) > 0.0 ? true : false;

    #Likelihood model for bayesian estimation
    e["likelihood"]["type"] = "perso"
    push!(e["likelihood"]["parameters"], Uniform(0.0, 2.0)) #An additional parameters for the likelihood
    e["likelihood"]["computational likelihood"] = noiseModel;

    #We use the optimsation algorithm CMA-ES
    e["task"] = "parameter estimation"
    e["algo"]["type"] = "CMAES";

    covMat = Matrix(1.0I, 7+1, 7+1);
    covMat[3,3] = covMat[6,6] =  0.1
    e["algo"]["variance"] = covMat;
    e["algo"]["sigma"] = 0.3
    e["algo"]["max iter"] = 300
    e["algo"]["lambda"] = 300 
    e["algo"]["ratio limite lambda"] = 3
    e["algo"]["min gen tol"] = 10
    e["algo"]["logTolFun"] = 1.0e-1
    e["algo"]["start"] = "distribution"
    e["algo"]["distribution"] =  zeros(8)
    e["algo"]["distribution"][1:7] = rand.(e["model"]["parameters"])
    e["algo"]["distribution"][8] = 0.1 #sigma
    e["algo"]["distribution"][2] = 0.3  #eta hom
    e["algo"]["distribution"][4] = 0 #Delta hom
    e["algo"]["distribution"][7] = 0 #Delta het
    e["algo"]["distribution"][5] = 0.3 #eta het

    @time run(e);

    ######### Display and save results ########
    #Directory fig/ and res/ have to be created

    #Plot the solution
    plt1 = plot_solution(e, t_plot=collect(1:3000), vect_params=e["results"]["parameters"], plot_data=false)
    plot!(title=string(name," - CMA-ES"))
    #Plot data
    scatter!(ti, ni_het./ni_tot, color=:blue, marker=:circle, label="data - immature  het")
    scatter!(ti, ni_hom./ni_tot, color=:green, marker=:diamond, label="data - immature  hom")
    scatter!(tm, ym, color=:black, marker=:dtriangle, label="data - mature allele burden")
    plt1.series_list[1].plotattributes[:label] = "model - immature het"
    plt1.series_list[2].plotattributes[:label] = "model - immature hom"
    plt1.series_list[3].plotattributes[:label] = "model - mature allele burden"
    savefig(string("fig/",name,"_solution.png")) #Save the graph

    #Check if there is a good convergence of the CMA-ES algorithm.
    #If not, restart (for the patients in that case) with more adequate values for the initial conditions: eta_het and eta_hom
    plt2 = plot_convergence_logPosterior(e)
    plot!(title=string(name))
    savefig(string("fig/",name,"_convergence_CMA-ES.png"))

    #Save results to use them for the hierarchical inference
    save(string("res/",name,"_CMAES.jld2"), Dict(
                          "m"=>e["results"]["m"], #Maximum Likelihood Estimator
                          "C"=>e["results"]["C"], #Covariance matrix
                          "sigma"=>e["results"]["sigma"], #Noise
                          "final lambda"=>e["results"]["final lambda"], #Parameter related to the CMA-ES algorithm
                          "g"=>e["results"]["g"]) #Final number of generations (related to CMA-ES algorithm)
                          )

end

