"""
noiseModel(X)
Personalised noise model for the data of the cohort
Combine a multiplicative noise for mature cells
With multinomial distributions for immature cells observations
"""
function noiseModel(X)
  
  #1- Mature - multiplicative noise
  l_mat = pdf(MvNormal(X["f"][3], sqrt.(X["f"][3].*(1.0.-X["f"][3])).*X["parameters likelihood"][1]), X["data"][1])

  #2- Immature - multinomial
  n_het = X["data"][3]
  n_hom = X["data"][4]
  n_wt = X["data"][2]
  y_het = X["f"][1]
  y_hom = X["f"][2]
  
  l_im_vect = [pdf(Multinomial(n_het[i]+n_hom[i]+n_wt[i],probability_vector(y_het[i], y_hom[i]) ), [n_het[i],n_hom[i],n_wt[i]]) for i in 1:length(n_wt)]
  l_im = prod(l_im_vect)
  return X["likelihood"] = l_mat*l_im
end

"""
function probability_vector 
ensures that Multinomial (in noiseModel) deals with exact probability vectors (sum = 1).
"""
function probability_vector(y_het, y_hom)
  y_het_corr = round(y_het, digits=20)
  y_hom_corr = round(y_hom, digits=20)
  y_wt_corr = 1.0 - y_het_corr - y_hom_corr
  if y_het == 1.0
    y_het_corr = 1.0-y_hom_corr
    y_wt_corr = 0.0
  elseif y_hom == 1.0
    y_hom_corr = 1.0 - y_het_corr
    y_wt_corr = 1.0 - y_het_corr - y_hom_corr
  elseif y_hom + y_het >= 1.0
    if y_het > y_hom
      y_het_corr = 1.0
      y_hom_corr = 0.0
      y_wt_corr = 0.0
    else
      y_hom_corr = 1.0
      y_het_corr = 0.0
      y_wt_corr = 0.0
    end
  elseif y_wt_corr < 0.0
    if y_het > y_hom
      y_het_corr = 1.0
      y_hom_corr = 0.0
      y_wt_corr = 0.0
    else
      y_hom_corr = 1.0
      y_het_corr = 0.0
      y_wt_corr = 0.0
    end

  end
  return [y_het_corr, y_hom_corr, y_wt_corr] #het, hom, wt
end

