###########################
### Analytical solution ###
### for the ODE system  ###
###########################

#Parameter structure (involved in the ODE system)
struct params
  gamma
  beta
  alpha
  Delta
  kappa_i
  delta_i
  kappa_m
  delta_m
end

#Discriminant
D(p) = (p.gamma+p.beta-p.alpha*p.Delta)^2 +4*p.gamma*p.alpha*p.Delta

function lambda(p)
  d = D(p)
  a = -(p.gamma+p.beta-p.alpha*p.Delta)/2
  b = 0
  if d >= 0
    b = sqrt(d)/2
  else
    b = 1/2 * sqrt(-d)*im
  end
  return (a+b, a-b)
end


"""
N_ODE
return the analytical solution [N1(t), N2(t), Ni(t), Nm(t)]
over time t
When Initial Conditions are N0
For parameters values p
"""
function N_ODE(t, p, N0)

  det = D(p)
  lp, lm = lambda(p)
    
  #N_1
  N1 = -p.beta/sqrt(det)*(
            ( (lm+p.gamma)/p.beta*N0[1] - N0[2])*exp.(lp.*t) -
            ((lp+p.gamma)/p.beta*N0[1] - N0[2])*exp.(lm.*t)
            ) 

  #N_2
  N2 = -p.beta/sqrt(det)*(
            ( (lm+p.gamma)*(lp+p.gamma)/p.beta^2*N0[1] - (lp+p.gamma)/p.beta*N0[2])*exp.(lp.*t) -
            ((lm+p.gamma)*(lp+p.gamma)/p.beta^2*N0[1] - (lm+p.gamma)/p.beta*N0[2])*exp.(lm.*t)
            ) 
    
  #N_i
  F = p.alpha*(1.0-p.Delta)*p.kappa_i
    
  Ap = -p.beta/sqrt(det)*( (lm+p.gamma)*(lp+p.gamma)/p.beta^2*N0[1] - (lp+p.gamma)/p.beta*N0[2])
  Am = +p.beta/sqrt(det)*((lm+p.gamma)*(lp+p.gamma)/p.beta^2*N0[1] - (lm+p.gamma)/p.beta*N0[2])
   
  Ki = N0[3] - F*(Ap/(lp+p.delta_i)+Am/(lm+p.delta_i))
  Ni = Ki*exp.(-p.delta_i .*t) +
       F*(Ap*exp.((lp).*t)/(lp+p.delta_i) +  Am*exp.((lm).*t)/(lm+p.delta_i))
    
  #N_m
  Km = N0[4] - p.delta_i*p.kappa_m*( Ki/(-p.delta_i+p.delta_m) +
                        F*(Ap/((lp+p.delta_m)*(lp+p.delta_i)) +  Am/((lm+p.delta_m)*(lm+p.delta_i)) ) )

  Nm = Km.*exp.(-p.delta_m .*t) .+
       p.delta_i*p.kappa_m*( Ki.*exp.(-p.delta_i.*t)/(-p.delta_i+p.delta_m) .+
                      F*(Ap/(lp+p.delta_i).*exp.(lp.*t)/(lp+p.delta_m)+
                                                        Am/(lm+p.delta_i).*exp.(lm.*t)/(lm+p.delta_m)))

  return [N1, N2, Ni , Nm]
end

