##################################
# Define the computational model #
##################################

include("solution1Brick.jl") #Analytical solution from one ODE system

function model(X)

  ###### Defintion of the parameters ##########
  #Before treatment
  gamma = 1/300.0;
  xi = 0.1;
  beta = gamma*(1.0-xi)/xi
  alpha = 1.0/30.0 #Rate for recruiting active HSCs
  delta_i = 1/6; #Rate at which progenitor cells differentiate
  delta_m = 1.0 #Death rate of mature cells (granulocyte) in days-1

  #Parameters related to WT cells - without IFNalpha
  alpha_ifn = 1.0/30.0;
  ki_ifn = 1.0;
  km_ifn = 1.0;

  #Parameters related to homozygous cells
  ki_hom = 1.0; 
  km_hom = X["parameters"][1]; #To be estimated
  eta_hom = X["parameters"][2]; #Initial condition - to be estimated
  chi_hom = gamma/(gamma+beta) 
  gamma_ifn_hom = X["parameters"][3]; #To be estimated
  beta_ifn_hom = beta;
  Delta_ifn_hom = X["parameters"][4]; #To be estimated
  ki_ifn_hom = ki_ifn;
  km_ifn_hom = km_ifn

  if X["input"]["homozygote"] == false
    #To ensure not having long-term dynamics in homozygous compartments if the patient has not homozygous mutated cells
    eta_hom = 0.0
  end

  #Parameters related to heterozygous cells
  ki_het = ki_hom
  km_het = km_hom
  eta_het = X["parameters"][5] #To be estimated
  chi_het= gamma/(gamma+beta)
  gamma_ifn_het = X["parameters"][6] #To be estimated
  beta_ifn_het = beta_ifn_hom
  Delta_ifn_het = X["parameters"][7] #To be estimated
  ki_ifn_het = ki_ifn
  km_ifn_het = km_ifn

  #### Obersvation times #####  
  ti_het = copy(X["t"][1]) #Observation times for immature cells
  ti = ti_hom = copy(X["t"][2]) #Has to be the same as ti_het
  tm = copy(X["t"][3]) #Observation time for mature cells

  ti = map( t -> t<0 ? t=0 : t, ti) #We consider observations before therapy as initial conditions (t=0)
  tm = map( t -> t<0 ? t=0 : t, tm)
  
  #### We use structure to define set of parameters according to the genotype, and wheter they refer to before of after the treatment (i.e., with or without IFNalpha)
  #params : gamma, beta, alpha, Delta, ki, delta_i, km, delta_m
  p = params(gamma, beta, alpha, 0.0, 1.0, delta_i, 1.0, delta_m)  #WT without IFN
  p_hom = params(p.gamma, p.beta, p.alpha, 0.0, ki_hom, p.delta_i,km_hom, p.delta_m) #hom without IFN
  p_het = params(p.gamma, p.beta, p.alpha, 0.0, ki_het, p.delta_i,km_het, p.delta_m) #het without IFN
  
  #Params with IFN
  p_ifn = params(p.gamma, p.beta, alpha_ifn, 0.0, ki_ifn, p.delta_i,km_ifn, p.delta_m)
  p_ifn_hom = params(gamma_ifn_hom, beta_ifn_hom, p_ifn.alpha, Delta_ifn_hom, ki_ifn_hom*p_hom.kappa_i,  p.delta_i, km_ifn_hom*p_hom.kappa_m, p.delta_m) 
  p_ifn_het = params(gamma_ifn_het, beta_ifn_het, p_ifn.alpha, Delta_ifn_het, ki_ifn_het*p_het.kappa_i,  p.delta_i, km_ifn_het*p_het.kappa_m, p.delta_m) 
    
  ### Initial conditions ####
  #Hom
  N0_2_hom = chi_hom*eta_hom #Compartment HSC 2
  N0_1_hom = eta_hom - N0_2_hom #Comp HSC 1
  N0_i_hom = p_hom.kappa_i*p_hom.alpha/p_hom.delta_i* N0_2_hom #Comp progenitors (or immatures i)
  N0_m_hom = p_hom.kappa_m*p_hom.delta_i/p_hom.delta_m*N0_i_hom #Comp matures m
  N0_hom = [N0_1_hom, N0_2_hom, N0_i_hom, N0_m_hom]

  #Het
  N0_2_het = chi_het*eta_het
  N0_1_het = eta_het - N0_2_het
  N0_i_het = p_het.kappa_i*p_het.alpha/p_het.delta_i* N0_2_het
  N0_m_het = p_het.kappa_m*p_het.delta_i/p_het.delta_m*N0_i_het
  N0_het = [N0_1_het, N0_2_het, N0_i_het, N0_m_het]

  #WT
  N0_1_WT = p.beta/(p.beta+p.gamma)
  N0_2_WT = p.gamma/(p.beta+p.gamma)
  N0_i_WT = p.kappa_i*p.alpha/p.delta_i* N0_2_WT
  N0_m_WT = p.kappa_m*p.delta_i/p.delta_m*N0_i_WT
  N0_WT = [N0_1_WT, N0_2_WT, N0_i_WT, N0_m_WT];

  ### Compute over time ####
  #Use analytical solution N_ODE
  #Immatures
  Nt_i_hom = X["input"]["homozygote"] ? N_ODE(ti, p_ifn_hom, N0_hom)[3] : 0.0 .* ti
  Nt_i_WT = N_ODE(ti, p_ifn, N0_WT)[3]
  Nt_i_het = N_ODE(ti, p_ifn_het, N0_het)[3] 
  #Matures
  Nt_m_hom = X["input"]["homozygote"] ? N_ODE(tm, p_ifn_hom, N0_hom)[4] : 0.0 .* tm
  Nt_m_WT = N_ODE(tm, p_ifn, N0_WT)[4]
  Nt_m_het = N_ODE(tm, p_ifn_het, N0_het)[4] #het
  #Compute CF for progenitor cells
  zi_het = Nt_i_het./(Nt_i_WT.+Nt_i_hom.+Nt_i_het)
  zi_hom = Nt_i_hom./(Nt_i_WT.+Nt_i_hom.+Nt_i_het)
  #Compute VAF for mature cells
  ym = (0.5.*Nt_m_het .+ Nt_m_hom)./(Nt_m_het.+Nt_m_hom.+Nt_m_WT)
  
  #Store theoretical results for comparison to data
  X["f"][1] = zi_het;
  X["f"][2] = zi_hom;
  X["f"][3] = ym;

end


