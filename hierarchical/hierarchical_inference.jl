#First of all, the Bayesian Inference Framework has to be called.
#Core functionnalities from this framework are available at:
#https://gitlab-research.centralesupelec.fr/2012hermangeg/bayesian-inference
#The individual inference should already have been run (using the CMA-ES algorithm)

include("../../../bayesian-inference/core/environment.jl") #Change the link accordingly



using LinearAlgebra
num_seed = 120
Random.seed!(num_seed) #seed to reproduce results

dir_data = "../data/"


#Include the computational model
include("../JAK2V617F_model/computationalModel.jl")

#Define the list of our 30 virtual patients
list_names = [string(i) for i in 1:30]


#Include the setting for the hierarchical inference
include("hierarchicalSetting.jl")


#Choose how many iterations we want
e["algo"]["max iter"] = 100_000

#We potentially tunned an hyper-parameter for each patient, for the Metropolis-Hasting step
cor_sig = Dict()
for name in list_names
  cor_sig[name] = 5.0e-7
end

nb_params = 8 #Number of parameters to estimate for each patient

#Initialize the algorithm
for i in 1:length(list_names)
  name = list_names[i]
  #We load the resuts from the previous step, that is the output of the CMA-ES algorthm
  res_CMAES = load(string("../CMAES/res/",name,"_CMAES.jld2"))

  #We define the covariance matrix of the Metropolis-Hasting proposal
  d = det(res_CMAES["C"])
  l = d^(-1/nb_params)
  e["algo"]["sub algo"][i]["variance"] = copy(Array(cor_sig[name].*l.* 0.5 .*(res_CMAES["C"]+res_CMAES["C"]')))
  #We choose to initialize the algrithm from the output of the CMA-ES algorithm
  e["algo"]["sub algo"][i]["initial distribution"] = copy(res_CMAES["m"])

end

#Run the procedure
@time run(e);

#Save results
new_res = Dict()
new_res["Hparameters"] = e["results"]
for i in 1:length(list_names)
  name = list_names[i]
  new_res[name] = e["sub env"][i]["results"]
end
save("results_hierarchical.jld2",new_res )
