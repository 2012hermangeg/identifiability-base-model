################################
### Hierarchical Settings ######
################################


using CSV, DataFrames
#Include the observation model
include("../observation_model/noiseModel.jl")

dir_data = "../data/"

######### Settings of the environment ##########
e = newEnvironment(seed=num_seed, hierarchical=true, nbInd=length(list_names))

#### Parameters and prior distributions

#Related to homozygous cells
push!(e["model"]["parameters"], H->truncated(Normal(H[1],sqrt(H[2])), 1.0, 20.0)) #km homozygous
push!(e["hyperparameters"],Uniform(1.0, 20.0))
push!(e["hyperparameters"],Uniform(0.0, 2.0))
push!(e["model"]["parameters"], Uniform(0.0, 3.0)) #eta hom
push!(e["model"]["parameters"], H->truncated(Normal(H[3],sqrt(H[4])), 1.0/300.0, 0.1)) #gamma ifn hom
push!(e["hyperparameters"],Uniform(1.0/300.0, 0.1))
push!(e["hyperparameters"],Uniform(0.0, 2.0))
push!(e["model"]["parameters"],H->truncated(Normal(H[5],sqrt(H[6])),-1.0, 1.0)) #Delta IFN hom
push!(e["hyperparameters"], Uniform(-1.0, 1.0))
push!(e["hyperparameters"],Uniform(0.0, 2.0))

#Related to heterozygous cells
push!(e["model"]["parameters"], Uniform(0.0, 3.0)) #eta het
push!(e["model"]["parameters"], H->truncated(Normal(H[7],sqrt(H[8])),1.0/300.0, 0.1)) #gamma ifn het
push!(e["hyperparameters"],Uniform(1.0/300.0, 0.1))
push!(e["hyperparameters"],Uniform(0.0, 2.0))
push!(e["model"]["parameters"], H->truncated(Normal(H[9],sqrt(H[10])),-1.0, 1.0)) #Delta IFN het
push!(e["hyperparameters"], Uniform(-1.0, 1.0))
push!(e["hyperparameters"],Uniform(0.0, 2.0))



#Computational model
e["model"]["computational model"] = model

######### Load Data ###########################
for i in 1:length(list_names)
  name = list_names[i]

  #Immature cells
  immat = CSV.read(string(dir_data,name,"_immat.csv"), DataFrame) #Data for immature cells - clonal architecture for progenitors
  ti = convert(Vector, immat[!,:t]) #Observation time for immature cells
  ni_het = convert(Vector{Int}, immat[!,:n_het]) #Number of heterozygous mutated cells for sample j
  ni_wt = convert(Vector{Int}, immat[!,:n_wt]) #Number of wildt-type cells for sample j
  ni_hom = convert(Vector{Int}, immat[!,:n_hom]) #Number of homozygous mutated cells for sample j
  ni_tot = ni_het .+ ni_hom .+ ni_wt
  #Mature cells
  mat = CSV.read(string(dir_data,name,"_mat.csv"), DataFrame) #Data form mature cells - VAF among granulocytes
  tm = convert(Vector, mat[!,:t]) #Observation time for mature cells
  ym = convert(Vector, mat[!,:y]) #VAF for mature cells

  push!(e["data"][i]["t"], copy(ti)) 
  push!(e["data"][i]["t"], copy(ti))  #Same as above, to have 3 time series, as many as model outputs
  push!(e["data"][i]["t"], copy(tm))
  push!(e["data"][i]["f"], copy(ym))
  push!(e["data"][i]["f"], copy(ni_wt))
  push!(e["data"][i]["f"], copy(ni_het))
  push!(e["data"][i]["f"], copy(ni_hom))
  
  #To take into account that some patients might no have any homozygous clones (thus, do not infer parameters related to homozygous cells)
  e["sub env"][i]["input"]["homozygote"] = maximum(ni_hom) > 0.0 ? true : false
end
##########################################


#Likelihood model for bayesian estimation
e["likelihood"]["type"] = "perso"
push!(e["likelihood"]["parameters"], H->truncated(Normal(H[11],sqrt(H[12])),0.0, 2.0)) #Here also, introduce Hyper-parameters
push!(e["hyperparameters"],Uniform(0.0, 2.0))
push!(e["hyperparameters"],Uniform(0.0, 2.0))

e["likelihood"]["computational likelihood"] = noiseModel

#Define the algorithm: Metropolis-Hasting within Gibbs
e["task"] = "parameter estimation"
e["algo"]["type"] = "Metropolis Hasting"

e["algo"]["Hparameters"]["type"] = "Gibbs"

#Need to express how works the Gibbs sampler
function gibbs_perso(e, iter)
  theta_mean = zeros(e["nbExtParameters"]-2)
  theta_L2 = zeros(e["nbExtParameters"]-2)
  
  Hparams_mu = e["results"]["sample"]["Hparameters"][collect(1:2:length(e["hyperparameters"])),iter-1] #Only odd numbers are gaussian means
  index_theta = vcat(collect(1:1),collect(3:4), collect(6:8))
  for p in 1:e["nb ind"]
    theta_p = e["sub env"][p]["results"]["sample"]["ExtParameters"][index_theta,iter-1]
    theta_mean .+= theta_p
    theta_L2 .+= (theta_p .- Hparams_mu).^2
  end
  theta_mean ./= e["nb ind"]
  
  #First, sigma
  alpha = -1+0.5*e["nb ind"] + 1 #So that sig2 follows an improper prior distribution: inverse-gamma(0,0)
  for i in collect(2:2:length(e["hyperparameters"]))
    beta = 0.5*theta_L2[Int(i/2)]
    e["results"]["sample"]["Hparameters"][i,iter] = rand(truncated(InverseGamma(alpha, beta), minimum(e["hyperparameters"][i]), maximum(e["hyperparameters"][i])))
  end

  #Then, the mean
  for i in collect(1:2:length(e["hyperparameters"]))
    e["results"]["sample"]["Hparameters"][i,iter] = rand(truncated(Normal(theta_mean[Int((i+1)/2)], sqrt(e["results"]["sample"]["Hparameters"][i+1,iter]/e["nb ind"])),
								minimum(e["hyperparameters"][i]), maximum(e["hyperparameters"][i])))
  end
  
end

e["algo"]["Hparameters"]["proposal"] = gibbs_perso


#Define a function that plot the solution
function plot_patient(i; theta=nothing)
  plt = plot_solution(e["sub env"][i], plot_data=false,t_plot=collect(1:3000), vect_params=theta)
  ti = e["data"][i]["t"][1]
  tm = e["data"][i]["t"][3]
  ym = e["data"][i]["f"][1] 
  ni_wt = e["data"][i]["f"][2] 
  ni_het = e["data"][i]["f"][3] 
  ni_hom = e["data"][i]["f"][4] 
  ni_tot = ni_wt .+ ni_het .+ ni_hom
  scatter!(ti, ni_het./ni_tot, color=:blue, marker=:circle, label="data - immature het")
  scatter!(ti, ni_hom./ni_tot, color=:green, marker=:diamond, label="data - immature hom")
  scatter!(tm, ym, color=:black, marker=:dtriangle, label="data - mature allele burden")
  plt.series_list[1].plotattributes[:label] = "model - immature het"
  plt.series_list[2].plotattributes[:label] = "model - immature hom"
  plt.series_list[3].plotattributes[:label] = "model - mature allele burden"
  return plt
end
